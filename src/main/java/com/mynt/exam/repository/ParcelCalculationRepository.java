package com.mynt.exam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mynt.exam.model.ParcelCalculation;

public interface ParcelCalculationRepository extends JpaRepository<ParcelCalculation,String> {
	List<ParcelCalculation> findAllByOrderByPriorityAsc();
}
