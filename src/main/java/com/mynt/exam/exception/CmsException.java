package com.mynt.exam.exception;

public class CmsException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CmsException() {
		super();
	}

	public CmsException(String message) {
		super(message);
	}

	public CmsException(String message, Throwable cause) {
		super(message, cause);
	}

	public CmsException(Throwable cause) {
		super(cause);
	}
}
