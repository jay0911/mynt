package com.mynt.exam.exception;

public class CalculationException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CalculationException() {
		super();
	}

	public CalculationException(String message) {
		super(message);
	}

	public CalculationException(String message, Throwable cause) {
		super(message, cause);
	}

	public CalculationException(Throwable cause) {
		super(cause);
	}
}
