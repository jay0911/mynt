package com.mynt.exam.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.mynt.exam.dto.response.Response;


@ControllerAdvice
public class ExceptionHandling {
	
	private final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandling.class);
	
	@SuppressWarnings("rawtypes")
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	@ExceptionHandler(CalculationException.class)
	public Response handleCalculationException(CalculationException e) {
		LOGGER.info(e.getMessage());
		return Response.failed(HttpStatus.BAD_REQUEST, e.getMessage());
	}
	
	@SuppressWarnings("rawtypes")
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	@ExceptionHandler(Exception.class)
	public Response handleException(Exception e) {
		LOGGER.info(e.getMessage());
		return Response.failed(e.getMessage());
	}
	


}
