package com.mynt.exam.constant;

public enum CmsTemplateVariable {
	VOLUME("{V}"),
	HEIGHT("{H}"),
	LENGTH("{L}"),
	WIDTH("{W}"),
	WEIGHT("{WT}");
	
	private String value;
	
	CmsTemplateVariable(String value) {
		this.setValue(value);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
