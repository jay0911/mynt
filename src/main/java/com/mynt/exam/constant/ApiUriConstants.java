package com.mynt.exam.constant;

public class ApiUriConstants {
	private ApiUriConstants() {}
	
	public static final String V1_PARCEL_CMS = "/v1/parcel-cms";
	public static final String V1_CALC_PARCEL = "/v1/parcel";
	
	public static final String LIST = "/list";
	public static final String CALC = "/calculate";
	
	public static final String SAVE = "/save";
	public static final String EDIT = "/edit";
	public static final String DELETE = "/delete/{id}";
}
