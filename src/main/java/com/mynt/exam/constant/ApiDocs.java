package com.mynt.exam.constant;

public class ApiDocs {
	private ApiDocs() {}
	public static final String PARCEL_CMS_API = "api maintainance of the calculation rules stored in the repository";
	public static final String PARCEL_CMS_API_LIST = "listing of rules stored in database";
	public static final String PARCEL_CMS_API_SAVE = "saving of rules stored in database";
	public static final String PARCEL_CMS_API_EDIT = "editing of rules stored in database";
	public static final String PARCEL_CMS_API_DELETE = "editing of rules stored in database, use id found in list api to delete";
	
	public static final String PARCEL_CMS_API_SAVE_REQUESTBODY = "cost and condition variable used templating for main computation of the parcel \r\n"
			+ "VOLUME-{V},\r\n" + 
			"	HEIGHT-{H},\r\n" + 
			"	LENGTH-{L},\r\n" + 
			"	WIDTH-{W},\r\n" + 
			"	WEIGHT-{WT} \r\n"
			+ " for rule(condition) or calculation(cost) is 'weight exceeds 50kg' = {WT} > 50 \r\n"
			+ " sample2 'php20 * weight' = 20 * {WT} \r\n"
			+ " javascript like computation must be added see sample 1 and 2 \r\n"
			+ " upon saving templated java script computation is transformed and validated if it is a valid computation \r\n "
			+ "eg, ({W} * {H})/20 turns into ({1} * {1})/20 (valid). \r\n"
			+ " 20 * s {WT} turns 20 * s 0 (not valid throws validation exception) \r\n"
			+ " Cannot also input same property inside the repository \r\n"+
			"if rule(condition) or calculation(cost) is empty you can use N/A, emptystring or null/property-removal";
	
	public static final String PARCEL_CMS_API_EDIT_REQUESTBODY = "same as saving rules but added id property to specify the object to edit. id can be retrieved in listing api \r\n"
			+ " rules --- please see saving rules";
	
	public static final String PARCEL_CMS_PROPS_ID = "get it from listing api";
	
	public static final String PARCEL_CMS_RULE_NAME = "rule name eg, Heavy parcel";
	
	public static final String PARCEL_CMS_REFER_TO_REQ_BODY = "refer to request body documentation";
	
	public static final String PARCEL_MIN_VALUE = "Please select positive numbers Only";
	
	public static final String PARCEL_PROPS_DISCOUNT = "value can be MYNT or GFI, to disregard discount remove the property on request body";
}
