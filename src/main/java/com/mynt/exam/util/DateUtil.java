package com.mynt.exam.util;

import java.time.LocalDate;

public class DateUtil {
	
	private DateUtil() {}
	
	public static LocalDate stringToDate(String date) {
		return LocalDate.parse(date);
	}
	
	public static boolean compareTodayDateBefore(LocalDate toCompare) {
		return LocalDate.now().isBefore(toCompare);
	}

}
