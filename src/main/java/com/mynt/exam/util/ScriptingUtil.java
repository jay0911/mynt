package com.mynt.exam.util;

import java.math.BigDecimal;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class ScriptingUtil {
	private ScriptingUtil() {}
	
	public static boolean getCondition(String condition) throws ScriptException {
		try {
		    ScriptEngineManager manager = new ScriptEngineManager();
		    ScriptEngine engine = manager.getEngineByName("js");
		    return Boolean.TRUE.equals(engine.eval(condition));
		}catch(Exception e) {
			throw new ScriptException("not a valid condition : "+ condition);
		}
	}
	
	public static BigDecimal getComputation(String costCalculation) throws ScriptException {
		try {
		    ScriptEngineManager mgr = new ScriptEngineManager();
		    ScriptEngine engine = mgr.getEngineByName("JavaScript");
		    return new BigDecimal(engine.eval(costCalculation).toString());
		}catch(Exception e) {
			throw new ScriptException("not a valid calculation for computation : "+ costCalculation);
		}
	}
}
