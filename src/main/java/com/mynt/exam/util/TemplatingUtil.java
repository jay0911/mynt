package com.mynt.exam.util;

import java.util.HashMap;
import java.util.Map;

import com.mynt.exam.constant.CmsTemplateVariable;
import com.mynt.exam.dto.TemplateDto;

public class TemplatingUtil {
	
	private TemplatingUtil() {}
	
	public static final String NOT_APPLICABLE = "N/A";
	
	public static String convertTemplate(TemplateDto templateDto) {
		
		if(templateDto.getTemplate().equals(NOT_APPLICABLE)) {
			return NOT_APPLICABLE;
		}
		
		Map<String,String> templateMap = new HashMap<>();
		templateMap.put(CmsTemplateVariable.WEIGHT.getValue(), templateDto.getWeight().toString());
		templateMap.put(CmsTemplateVariable.VOLUME.getValue(),  templateDto.getVolume().toString());
		templateMap.put(CmsTemplateVariable.HEIGHT.getValue(), templateDto.getHeight().toString());
		templateMap.put(CmsTemplateVariable.LENGTH.getValue(), templateDto.getLength().toString());
		templateMap.put(CmsTemplateVariable.WIDTH.getValue(), templateDto.getWidth().toString());
		
		String template = templateDto.getTemplate();
		
		for (Map.Entry<String, String> entry : templateMap.entrySet()) {
			template = template.replace(entry.getKey(),entry.getValue());
		}

		return template;
	}
}
