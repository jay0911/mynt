package com.mynt.exam.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "product")
@AttributeOverride(name = "id", column = @Column(name = "id_parcel_calculation", length = 36, nullable = false))
public class ParcelCalculation extends StandardAudibleEntity {
	
	@Column(unique = true)
	private Integer priority;
	private String ruleName;
	private String condition;
	private String cost;
	
	public ParcelCalculation() {}
	
	public ParcelCalculation(Integer priority,String ruleName,String condition,String cost) {
		this.priority = priority;
		this.ruleName = ruleName;
		this.condition = condition;
		this.cost = cost;
	}
	
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}

}
