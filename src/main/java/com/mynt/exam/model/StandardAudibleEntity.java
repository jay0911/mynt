package com.mynt.exam.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
@EntityListeners(AuditableEntityListener.class)
public class StandardAudibleEntity extends BaseEntity {
	
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dtime_updated")
    private Date dtimeUpdated;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dtime_created")
    private Date dtimeCreated;
    
	public Date getDtimeUpdated() {
		return dtimeUpdated;
	}

	public void setDtimeUpdated(Date dtimeUpdated) {
		this.dtimeUpdated = dtimeUpdated;
	}

	public Date getDtimeCreated() {
		return dtimeCreated;
	}

	public void setDtimeCreated(Date dtimeCreated) {
		this.dtimeCreated = dtimeCreated;
	}
}
