package com.mynt.exam.model;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class AuditableEntityListener {
    @PrePersist
    public void prePersist(StandardAudibleEntity e) {
        Date now = new Date();

        e.setDtimeCreated(now);
        e.setDtimeUpdated(now);
    }

    @PreUpdate
    public void preUpdate(StandardAudibleEntity e) {
        e.setDtimeUpdated(new Date());
    }

}
