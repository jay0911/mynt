package com.mynt.exam.dto.response;

public class RuleResponse {
	
	private String id;
	private String dtimeCreated;
	private String dtimeUpdated;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDtimeCreated() {
		return dtimeCreated;
	}
	public void setDtimeCreated(String dtimeCreated) {
		this.dtimeCreated = dtimeCreated;
	}
	public String getDtimeUpdated() {
		return dtimeUpdated;
	}
	public void setDtimeUpdated(String dtimeUpdated) {
		this.dtimeUpdated = dtimeUpdated;
	}
}
