package com.mynt.exam.dto.response;

/**
 * can be used for auditing
 * @author John.Oliveros
 *
 */
public class CalculationResponse {
	
	private String receipt;
	private String dtimeCreated;
	
	public String getReceipt() {
		return receipt;
	}
	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}
	public String getDtimeCreated() {
		return dtimeCreated;
	}
	public void setDtimeCreated(String dtimeCreated) {
		this.dtimeCreated = dtimeCreated;
	}
}
