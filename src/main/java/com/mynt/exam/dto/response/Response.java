package com.mynt.exam.dto.response;

import org.springframework.http.HttpStatus;

public class Response<T> {

    private String status;
    private int statusCode;
    private T data;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private Response(String status, int statusCode, String message, T data) {
        this.status = status;
        this.statusCode = statusCode;
        this.message = message;
        this.data = data;
    }

    public static <T> Response<T> success(T data) {
        return success(HttpStatus.OK, null, data);
    }

    public static <T> Response<T> success(HttpStatus httpStatus, String message) {
        return success(httpStatus, message, null);
    }

    public static <T> Response<T> success(HttpStatus httpStatus, String message, T data) {
        return new Response<>(httpStatus.getReasonPhrase(), httpStatus.value(), message, data);
    }

    public static <T> Response<T> failed(String message) {
        return failed(HttpStatus.BAD_REQUEST, message);
    }

    public static <T> Response<T> failed(HttpStatus httpStatus, String message) {
        return new Response<>(httpStatus.getReasonPhrase(), httpStatus.value(), message, null);
    }
}
