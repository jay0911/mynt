package com.mynt.exam.dto.response;

import java.math.BigDecimal;

public class ParcelCalculationResponse extends CalculationResponse {
	private BigDecimal costOfDelivery;

	public BigDecimal getCostOfDelivery() {
		return costOfDelivery;
	}

	public void setCostOfDelivery(BigDecimal costOfDelivery) {
		this.costOfDelivery = costOfDelivery;
	}
}
