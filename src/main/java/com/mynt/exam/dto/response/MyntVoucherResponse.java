package com.mynt.exam.dto.response;

import java.math.BigDecimal;

public class MyntVoucherResponse {
	
	private String expiry;
	private String code;
	private BigDecimal discount;
	
	public String getExpiry() {
		return expiry;
	}
	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	
	
	@Override
	public String toString() {
		return "MyntVoucherResponse [expiry=" + expiry + ", code=" + code + ", discount=" + discount + "]";
	}
}
