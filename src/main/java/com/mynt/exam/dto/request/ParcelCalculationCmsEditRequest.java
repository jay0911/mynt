package com.mynt.exam.dto.request;

import com.mynt.exam.constant.ApiDocs;

import io.swagger.annotations.ApiModelProperty;

public class ParcelCalculationCmsEditRequest extends CalculationEditCmsRequest {
	
	@ApiModelProperty(example = ApiDocs.PARCEL_CMS_PROPS_ID)
	private String id;
	
	private Integer priority;
	
	@ApiModelProperty(example = ApiDocs.PARCEL_CMS_RULE_NAME)
	private String ruleName;
	
	@ApiModelProperty(example = ApiDocs.PARCEL_CMS_REFER_TO_REQ_BODY)
	private String condition;
	
	@ApiModelProperty(example = ApiDocs.PARCEL_CMS_REFER_TO_REQ_BODY)
	private String cost;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
}
