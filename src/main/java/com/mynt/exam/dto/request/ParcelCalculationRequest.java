package com.mynt.exam.dto.request;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

import com.mynt.exam.constant.ApiDocs;
import com.mynt.exam.constant.VoucherCode;

import io.swagger.annotations.ApiModelProperty;

public class ParcelCalculationRequest extends CalculationRequest {
	
	@NotNull
	@Range(min = 0l, message = ApiDocs.PARCEL_MIN_VALUE)
	private BigDecimal weight;
	
	@NotNull
	@Range(min = 0l, message = ApiDocs.PARCEL_MIN_VALUE)
	private BigDecimal height;
	
	@NotNull
	@Range(min = 0l, message = ApiDocs.PARCEL_MIN_VALUE)
	private BigDecimal width;
	
	@NotNull
	@Range(min = 0l, message = ApiDocs.PARCEL_MIN_VALUE)
	private BigDecimal length;
	
	@ApiModelProperty(example = ApiDocs.PARCEL_PROPS_DISCOUNT)
	private VoucherCode voucherCode;
	
	public BigDecimal getWeight() {
		return weight;
	}
	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}
	public BigDecimal getHeight() {
		return height;
	}
	public void setHeight(BigDecimal height) {
		this.height = height;
	}
	public BigDecimal getWidth() {
		return width;
	}
	public void setWidth(BigDecimal width) {
		this.width = width;
	}
	public BigDecimal getLength() {
		return length;
	}
	public void setLength(BigDecimal length) {
		this.length = length;
	}
	public VoucherCode getVoucherCode() {
		return voucherCode;
	}
	public void setVoucherCode(VoucherCode voucherCode) {
		this.voucherCode = voucherCode;
	}
}
