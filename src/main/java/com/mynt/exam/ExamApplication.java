package com.mynt.exam;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.mynt.exam.util.SSLUtil;

@SpringBootApplication
@ComponentScan("com.mynt.exam.*")
public class ExamApplication {

	public static void main(String[] args) throws KeyManagementException, NoSuchAlgorithmException {
		SpringApplication.run(ExamApplication.class, args);
		SSLUtil.turnOffSslChecking();
	}

}
