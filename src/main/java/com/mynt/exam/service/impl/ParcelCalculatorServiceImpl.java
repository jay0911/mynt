package com.mynt.exam.service.impl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import javax.script.ScriptException;

import org.springframework.stereotype.Service;

import com.mynt.exam.adapter.MyntVoucherAdapter;
import com.mynt.exam.constant.VoucherCode;
import com.mynt.exam.dto.TemplateDto;
import com.mynt.exam.dto.request.CalculationRequest;
import com.mynt.exam.dto.request.ParcelCalculationRequest;
import com.mynt.exam.dto.response.CalculationResponse;
import com.mynt.exam.dto.response.MyntVoucherResponse;
import com.mynt.exam.dto.response.ParcelCalculationResponse;
import com.mynt.exam.exception.CalculationException;
import com.mynt.exam.model.ParcelCalculation;
import com.mynt.exam.repository.ParcelCalculationRepository;
import com.mynt.exam.service.CalculatorService;
import com.mynt.exam.util.DateUtil;
import com.mynt.exam.util.ScriptingUtil;
import com.mynt.exam.util.TemplatingUtil;

@Service("ParcelCalculatorServiceImpl")
public class ParcelCalculatorServiceImpl implements CalculatorService {
	
	private MyntVoucherAdapter myntVoucherAdapter;
	private ParcelCalculationRepository parcelCalculationRepository;
	
	public ParcelCalculatorServiceImpl(MyntVoucherAdapter myntVoucherAdapter,
			ParcelCalculationRepository parcelCalculationRepository) {
		this.myntVoucherAdapter = myntVoucherAdapter;
		this.parcelCalculationRepository = parcelCalculationRepository;
	}
	
	@Override
	public CalculationResponse calculate(CalculationRequest calculationRequest) throws ScriptException, CalculationException {
		ParcelCalculationRequest parcelRequest = (ParcelCalculationRequest) calculationRequest;
		ParcelCalculationResponse response = initResponse();	

		List<ParcelCalculation> parcelRules = parcelCalculationRepository.findAllByOrderByPriorityAsc();
		validateAndCalcEveryParcelRule(parcelRules, parcelRequest, response);
		
		return response;
	}
	
	private void validateAndCalcEveryParcelRule(List<ParcelCalculation> parcelRules,ParcelCalculationRequest parcelRequest ,ParcelCalculationResponse response) throws ScriptException, CalculationException {
		for(ParcelCalculation parcelRule : parcelRules) {
			String condition = getTemplate(parcelRequest, parcelRule.getCondition());
			String costComputation = getTemplate(parcelRequest, parcelRule.getCost());

			if(validateCondition(parcelRule, condition)) {
				checkIfParcelIsRejected(costComputation);
				
				BigDecimal result = ScriptingUtil.getComputation(costComputation);			
				BigDecimal discount = getDiscount(parcelRequest.getVoucherCode());
				BigDecimal divideAnswer = discount.divide(new BigDecimal("100"));
				BigDecimal deductToPrice = divideAnswer.multiply(result);

				response.setCostOfDelivery((result.subtract(deductToPrice)).setScale(2, BigDecimal.ROUND_UP));
				return;
			}
		}
	}
	
	private void checkIfParcelIsRejected(String costComputation) throws CalculationException {
		if(costComputation.equals(TemplatingUtil.NOT_APPLICABLE)) {
			throw new CalculationException("parcel rejected");
		}
	}
	
	private boolean validateCondition(ParcelCalculation parcelRule, String condition) throws ScriptException {
		return parcelRule.getCondition().equals(TemplatingUtil.NOT_APPLICABLE) || ScriptingUtil.getCondition(condition);
	}
	
	private ParcelCalculationResponse initResponse() {
		ParcelCalculationResponse response = new ParcelCalculationResponse();
		response.setDtimeCreated(LocalDateTime.now().toString());
		response.setReceipt(LocalDateTime.now().toString().concat("-").concat(UUID.randomUUID().toString()));
		return response;
	}
	
	private String getTemplate(ParcelCalculationRequest parcelRequest, String template) {
		TemplateDto templateDto = new TemplateDto();
		templateDto.setHeight(parcelRequest.getHeight());
		templateDto.setLength(parcelRequest.getLength());
		templateDto.setTemplate(template);
		templateDto.setVolume(parcelRequest.getHeight().multiply(parcelRequest.getWidth()).multiply(parcelRequest.getLength()));
		templateDto.setWeight(parcelRequest.getWeight());
		templateDto.setWidth(parcelRequest.getWidth());
		return TemplatingUtil.convertTemplate(templateDto);
	}
	
	private BigDecimal getDiscount(VoucherCode code) {
		if(code != null) {
			MyntVoucherResponse response = myntVoucherAdapter.getDiscount(code);
			if(DateUtil.compareTodayDateBefore(DateUtil.stringToDate(response.getExpiry()))) {
				return response.getDiscount();
			}		
		}
		return new BigDecimal(0);
	}

}
