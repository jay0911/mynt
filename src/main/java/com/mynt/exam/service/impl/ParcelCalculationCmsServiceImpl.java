package com.mynt.exam.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.script.ScriptException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.mynt.exam.dto.TemplateDto;
import com.mynt.exam.dto.request.CalculationEditCmsRequest;
import com.mynt.exam.dto.request.CalculationSaveCmsRequest;
import com.mynt.exam.dto.request.ParcelCalculationCmsEditRequest;
import com.mynt.exam.dto.request.ParcelCalculationCmsSaveRequest;
import com.mynt.exam.dto.response.ParcelRuleResponse;
import com.mynt.exam.dto.response.RuleResponse;
import com.mynt.exam.exception.CmsException;
import com.mynt.exam.model.ParcelCalculation;
import com.mynt.exam.repository.ParcelCalculationRepository;
import com.mynt.exam.service.CalculationCmsService;
import com.mynt.exam.util.ScriptingUtil;
import com.mynt.exam.util.TemplatingUtil;

@Service("ParcelCalculationCmsServiceImpl")
public class ParcelCalculationCmsServiceImpl implements CalculationCmsService {
	
	private final Logger LOGGER = LoggerFactory.getLogger(ParcelCalculationCmsServiceImpl.class);
	
	private ParcelCalculationRepository parcelCalculationRepository;
	
	public ParcelCalculationCmsServiceImpl(ParcelCalculationRepository parcelCalculationRepository) {
		this.parcelCalculationRepository = parcelCalculationRepository;
	}
	
	/**
	 * inits value to the database
	 */
	@PostConstruct
    public void initValues(){
		LOGGER.info("initialize values");
		ParcelCalculation reject = new ParcelCalculation(1, "Reject", "{WT} > 50", "N/A"); 
		ParcelCalculation heavyParcel = new ParcelCalculation(2, "Heavy Parcel", "{WT} > 10", "20 * {WT}"); 
		ParcelCalculation smallParcel = new ParcelCalculation(3, "Small Parcel", "{V} < 1500", "0.03 * {V}");
		ParcelCalculation mediumParcel = new ParcelCalculation(4, "Medium Parcel", "{V} < 2500", "0.04 * {V}");
		ParcelCalculation largeParcel = new ParcelCalculation(5, "Large Parcel", "N/A", "0.05 * {V}");
		
		List<ParcelCalculation> parcels = new ArrayList<>();
		parcels.add(reject);
		parcels.add(heavyParcel);
		parcels.add(smallParcel);
		parcels.add(mediumParcel);
		parcels.add(largeParcel);
		
		parcelCalculationRepository.saveAll(parcels);
		
		LOGGER.info("done initializing values");
	}
	
	public List<RuleResponse> findAll(){
		return parcelCalculationRepository.findAll()
				.stream()
				.map(u->{
					ParcelRuleResponse resp = new ParcelRuleResponse();
					resp.setCondition(u.getCondition());
					resp.setCost(u.getCost());
					resp.setDtimeCreated(u.getDtimeCreated().toString());
					resp.setDtimeUpdated(u.getDtimeUpdated().toString());
					resp.setId(u.getId());
					resp.setPriority(u.getPriority());
					resp.setRuleName(u.getRuleName());
					return resp;
				}).collect(Collectors.toList());
	}

	@Override
	public boolean save(CalculationSaveCmsRequest calculationSaveCmsRequest) throws ScriptException {
		ParcelCalculationCmsSaveRequest request = (ParcelCalculationCmsSaveRequest) calculationSaveCmsRequest;
		
		validateCondition(request.getCondition());
		validateComputation(request.getCost());
		
		ParcelCalculation calc = new ParcelCalculation();
		calc.setCondition(validateCondition(request.getCondition()));
		calc.setCost(validateComputation(request.getCost()));
		calc.setPriority(request.getPriority());
		calc.setRuleName(request.getRuleName());
		try {
			parcelCalculationRepository.save(calc);		
		}catch(Exception e) {
			LOGGER.info("saving failed :".concat(e.getMessage()));	
			return false;
		}
		return true;
		
	}

	@Override
	public boolean edit(CalculationEditCmsRequest calculationEditCmsRequest) throws ScriptException, CmsException {
		ParcelCalculationCmsEditRequest request = (ParcelCalculationCmsEditRequest) calculationEditCmsRequest;
		
		ParcelCalculation calc = parcelCalculationRepository.findById(request.getId())
				.orElseThrow(()->new CmsException("id not found"));
		
		calc.setCondition(validateCondition(request.getCondition()));
		calc.setCost(validateComputation(request.getCost()));
		calc.setPriority(request.getPriority());
		calc.setRuleName(request.getRuleName());
		try {
			parcelCalculationRepository.save(calc);		
		}catch(Exception e) {
			LOGGER.info("editing failed :".concat(e.getMessage()));	
			return false;
		}
		return true;		
	}
	
	@Override
	public boolean delete(String id) throws CmsException {
		ParcelCalculation calc = parcelCalculationRepository.findById(id)
				.orElseThrow(()->new CmsException("id not found"));
		
		try {
			parcelCalculationRepository.delete(calc);
		}catch(Exception e) {
			LOGGER.info("deletion failed :".concat(e.getMessage()));	
			return false;
		}
		return true;
	}
	
	private String initTemplateForChecking(String template) {
		TemplateDto templateDto = new TemplateDto();
		templateDto.setHeight(new BigDecimal(1));
		templateDto.setLength(new BigDecimal(1));
		templateDto.setTemplate(template);
		templateDto.setVolume(new BigDecimal(1));
		templateDto.setWeight(new BigDecimal(1));
		templateDto.setWidth(new BigDecimal(1));
		return TemplatingUtil.convertTemplate(templateDto);
	}
	
	private String validateCondition(String condition) throws ScriptException {
		
		condition = setDefaultRule(condition);
		
		condition = initTemplateForChecking(condition);
		if(TemplatingUtil.NOT_APPLICABLE.equals(condition)) {
			return condition;
		}
		ScriptingUtil.getCondition(condition);
		
		return condition;
	}
	
	private String validateComputation(String costCalculation) throws ScriptException {
		
		costCalculation = setDefaultRule(costCalculation);
	
		costCalculation = initTemplateForChecking(costCalculation);
		if(TemplatingUtil.NOT_APPLICABLE.equals(costCalculation)) {
			return costCalculation;
		}
		ScriptingUtil.getComputation(costCalculation);
		
		return costCalculation;
	}
	
	private String setDefaultRule(String rule) {
		if(rule==null || rule.equals("")) {
			rule = TemplatingUtil.NOT_APPLICABLE;
		}
		return rule;
	}

}
