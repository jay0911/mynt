package com.mynt.exam.service;

import java.util.List;

import com.mynt.exam.dto.request.CalculationEditCmsRequest;
import com.mynt.exam.dto.request.CalculationSaveCmsRequest;
import com.mynt.exam.dto.response.RuleResponse;
import com.mynt.exam.exception.CmsException;

/**
 * 
 * @author John.Oliveros
 * 
 * generic inteface for a calculator cms
 *
 */
public interface CalculationCmsService {
	public List<? extends RuleResponse> findAll();
	public boolean save(CalculationSaveCmsRequest calculationSaveCmsRequest) throws Exception;
	public boolean edit(CalculationEditCmsRequest calculationSaveCmsRequest) throws Exception;
	public boolean delete(String id) throws CmsException;
}	
