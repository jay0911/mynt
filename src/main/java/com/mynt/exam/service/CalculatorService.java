package com.mynt.exam.service;

import com.mynt.exam.dto.request.CalculationRequest;
import com.mynt.exam.dto.response.CalculationResponse;

/**
 * generic interface for a calculator service
 * @author John.Oliveros
 *
 */
public interface CalculatorService {
	public CalculationResponse calculate(CalculationRequest calculationRequest) throws Exception;
}
