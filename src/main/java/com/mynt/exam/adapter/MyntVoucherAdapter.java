package com.mynt.exam.adapter;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.mynt.exam.constant.VoucherCode;
import com.mynt.exam.dto.response.MyntVoucherResponse;
import com.mynt.exam.properties.MyntVoucherProperties;

@Component
public class MyntVoucherAdapter {
	
	@Autowired
	private MyntVoucherProperties myntVoucherProperties;
	
	private final Logger LOGGER = LoggerFactory.getLogger(MyntVoucherAdapter.class);
	
	public MyntVoucherResponse getDiscount(VoucherCode code) {
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromUriString(myntVoucherProperties.getDefaultUri().concat(myntVoucherProperties.getContextPath()))
		        .queryParam("key", myntVoucherProperties.getApiKey());
		
		RestTemplate rt = new RestTemplate();
		
		MyntVoucherResponse resp = rt.exchange(builder.buildAndExpand(buildUrlParam(code)).toUri() , HttpMethod.GET,
		        null, MyntVoucherResponse.class).getBody();
		LOGGER.info(Marker.ANY_MARKER,"Result {0}.", resp); 
		return resp;
	}
	
	private Map<String, String> buildUrlParam(VoucherCode code){
		Map<String, String> urlParams = new HashMap<>();
		urlParams.put("voucherCode", code.toString());
		return urlParams;
	}
}
