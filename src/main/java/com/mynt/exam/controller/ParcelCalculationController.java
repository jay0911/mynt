package com.mynt.exam.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mynt.exam.constant.ApiUriConstants;
import com.mynt.exam.dto.request.ParcelCalculationRequest;
import com.mynt.exam.dto.response.Response;
import com.mynt.exam.service.CalculatorService;

@RestController
@RequestMapping(ApiUriConstants.V1_CALC_PARCEL)
public class ParcelCalculationController {
	
	private CalculatorService calculatorService;
	
	public ParcelCalculationController(@Qualifier("ParcelCalculatorServiceImpl")CalculatorService calculatorService) {
		this.calculatorService = calculatorService;
	}

	@PostMapping(ApiUriConstants.CALC)
	public Response<?> calculate(@Valid @RequestBody ParcelCalculationRequest request) throws Exception{
		return Response.success(calculatorService.calculate(request));
	}
}
