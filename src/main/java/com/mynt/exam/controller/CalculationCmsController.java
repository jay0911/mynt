package com.mynt.exam.controller;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mynt.exam.constant.ApiUriConstants;
import com.mynt.exam.constant.ApiDocs;
import com.mynt.exam.dto.request.ParcelCalculationCmsEditRequest;
import com.mynt.exam.dto.request.ParcelCalculationCmsSaveRequest;
import com.mynt.exam.dto.response.Response;
import com.mynt.exam.service.CalculationCmsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * simple crud for maintaining values on parcel calculation
 * @author John.Oliveros
 *
 */
@Api(ApiDocs.PARCEL_CMS_API)
@RestController
@RequestMapping(ApiUriConstants.V1_PARCEL_CMS)
public class CalculationCmsController {
	
	private CalculationCmsService calculationCmsService;
	
	public CalculationCmsController(@Qualifier("ParcelCalculationCmsServiceImpl") CalculationCmsService calculationCmsService) {
		this.calculationCmsService = calculationCmsService;
	}
	
	@ApiOperation(ApiDocs.PARCEL_CMS_API_LIST)
	@GetMapping(ApiUriConstants.LIST)
	public Response<?> list(){
		return Response.success(calculationCmsService.findAll());
	}
	
	@ApiOperation(ApiDocs.PARCEL_CMS_API_EDIT)
	@PostMapping(ApiUriConstants.EDIT)
	public Response<?> update(@ApiParam(ApiDocs.PARCEL_CMS_API_EDIT_REQUESTBODY) @RequestBody ParcelCalculationCmsEditRequest calculationEditCmsRequest) throws Exception {
		calculationCmsService.edit(calculationEditCmsRequest);
		return Response.success(null);
	}
	
	@ApiOperation(ApiDocs.PARCEL_CMS_API_SAVE)
	@PutMapping(ApiUriConstants.SAVE)
	public Response<?> save(@ApiParam(ApiDocs.PARCEL_CMS_API_SAVE_REQUESTBODY) @RequestBody ParcelCalculationCmsSaveRequest calculationSaveCmsRequest) throws Exception {
		calculationCmsService.save(calculationSaveCmsRequest);
		return Response.success(null);
	}
	
	@ApiOperation(ApiDocs.PARCEL_CMS_API_DELETE)
	@DeleteMapping(ApiUriConstants.DELETE)
	public Response<?> delete(@PathVariable String id) throws Exception {
		calculationCmsService.delete(id);
		return Response.success(null);
	}

}
