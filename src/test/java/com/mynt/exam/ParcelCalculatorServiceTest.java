package com.mynt.exam;

import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptException;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.mynt.exam.adapter.MyntVoucherAdapter;
import com.mynt.exam.constant.VoucherCode;
import com.mynt.exam.dto.request.ParcelCalculationRequest;
import com.mynt.exam.dto.response.MyntVoucherResponse;
import com.mynt.exam.dto.response.ParcelCalculationResponse;
import com.mynt.exam.exception.CalculationException;
import com.mynt.exam.model.ParcelCalculation;
import com.mynt.exam.repository.ParcelCalculationRepository;
import com.mynt.exam.service.impl.ParcelCalculatorServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ParcelCalculatorServiceTest {
	
	@InjectMocks
	private ParcelCalculatorServiceImpl parcelCalculatorServiceImpl;
	
	@Mock
	private MyntVoucherAdapter myntVoucherAdapter;
	
	@Mock
	private ParcelCalculationRepository parcelCalculationRepository;
	
	@Before
	public void setup(){
	    MockitoAnnotations.initMocks(this);
	}
	
	private List<ParcelCalculation> createParcelRules(){
		ParcelCalculation reject = new ParcelCalculation(1, "Reject", "{WT} > 50", "N/A"); 
		ParcelCalculation heavyParcel = new ParcelCalculation(2, "Heavy Parcel", "{WT} > 10", "20 * {WT}"); 
		ParcelCalculation smallParcel = new ParcelCalculation(3, "Small Parcel", "{V} < 1500", "0.03 * {V}");
		ParcelCalculation mediumParcel = new ParcelCalculation(4, "Medium Parcel", "{V} < 2500", "0.04 * {V}");
		ParcelCalculation largeParcel = new ParcelCalculation(5, "Large Parcel", "N/A", "0.05 * {V}");
		
		List<ParcelCalculation> parcels = new ArrayList<>();
		parcels.add(reject);
		parcels.add(heavyParcel);
		parcels.add(smallParcel);
		parcels.add(mediumParcel);
		parcels.add(largeParcel);
		return parcels;
	}
	
	private MyntVoucherResponse buildMyntVoucherCodeNotExpired() {
		MyntVoucherResponse resp = new MyntVoucherResponse();
		resp.setCode("MYNT");
		resp.setDiscount(new BigDecimal(12.25));
		resp.setExpiry("2021-05-10");
		return resp;
	}
	
	private MyntVoucherResponse buildGFIVoucherCodeNotExpired() {
		MyntVoucherResponse resp = new MyntVoucherResponse();
		resp.setCode("GFI");
		resp.setDiscount(new BigDecimal(7.5));
		resp.setExpiry("2021-05-10");
		return resp;
	}
	
	private MyntVoucherResponse buildMyntVoucherCodeExpired() {
		MyntVoucherResponse resp = new MyntVoucherResponse();
		resp.setCode("MYNT");
		resp.setDiscount(new BigDecimal(12.25));
		resp.setExpiry("2019-05-10");
		return resp;
	}
	
	@Test(expected=CalculationException.class)
	public void testPrio1() throws ScriptException, CalculationException {
		when(parcelCalculationRepository.findAllByOrderByPriorityAsc()).thenReturn(createParcelRules());
		ParcelCalculationRequest parcelRequest = new ParcelCalculationRequest();
		parcelRequest.setHeight(new BigDecimal("20"));
		parcelRequest.setLength(new BigDecimal("20"));
		parcelRequest.setVoucherCode(VoucherCode.MYNT);
		parcelRequest.setWeight(new BigDecimal("51"));
		parcelRequest.setWidth(new BigDecimal("20"));
		
		parcelCalculatorServiceImpl.calculate(parcelRequest);
	}
	
	@Test
	public void testPrio2() throws ScriptException, CalculationException {
		when(parcelCalculationRepository.findAllByOrderByPriorityAsc()).thenReturn(createParcelRules());
		when(myntVoucherAdapter.getDiscount(VoucherCode.MYNT)).thenReturn(buildMyntVoucherCodeNotExpired());
		ParcelCalculationRequest parcelRequest = new ParcelCalculationRequest();
		parcelRequest.setHeight(new BigDecimal("20"));
		parcelRequest.setLength(new BigDecimal("20"));
		parcelRequest.setVoucherCode(VoucherCode.MYNT);
		parcelRequest.setWeight(new BigDecimal("11"));
		parcelRequest.setWidth(new BigDecimal("20"));
		
		ParcelCalculationResponse resp = (ParcelCalculationResponse)parcelCalculatorServiceImpl.calculate(parcelRequest);
		
		Assertions.assertThat(resp.getCostOfDelivery()).isEqualTo(new BigDecimal("193.05"));
	}
	
	@Test
	public void testPrio3() throws ScriptException, CalculationException {
		when(parcelCalculationRepository.findAllByOrderByPriorityAsc()).thenReturn(createParcelRules());
		when(myntVoucherAdapter.getDiscount(VoucherCode.MYNT)).thenReturn(buildMyntVoucherCodeNotExpired());
		ParcelCalculationRequest parcelRequest = new ParcelCalculationRequest();
		parcelRequest.setHeight(new BigDecimal("2"));
		parcelRequest.setLength(new BigDecimal("500"));
		parcelRequest.setVoucherCode(VoucherCode.MYNT);
		parcelRequest.setWeight(new BigDecimal("2"));
		parcelRequest.setWidth(new BigDecimal("1"));
		
		ParcelCalculationResponse resp = (ParcelCalculationResponse)parcelCalculatorServiceImpl.calculate(parcelRequest);
		
		Assertions.assertThat(resp.getCostOfDelivery()).isEqualTo(new BigDecimal("26.33"));
	}
	
	@Test
	public void testPrio4() throws ScriptException, CalculationException {
		when(parcelCalculationRepository.findAllByOrderByPriorityAsc()).thenReturn(createParcelRules());
		when(myntVoucherAdapter.getDiscount(VoucherCode.MYNT)).thenReturn(buildMyntVoucherCodeNotExpired());
		ParcelCalculationRequest parcelRequest = new ParcelCalculationRequest();
		parcelRequest.setHeight(new BigDecimal("1"));
		parcelRequest.setLength(new BigDecimal("2400"));
		parcelRequest.setVoucherCode(VoucherCode.MYNT);
		parcelRequest.setWeight(new BigDecimal("2"));
		parcelRequest.setWidth(new BigDecimal("1"));
		
		ParcelCalculationResponse resp = (ParcelCalculationResponse)parcelCalculatorServiceImpl.calculate(parcelRequest);
		
		Assertions.assertThat(resp.getCostOfDelivery()).isEqualTo(new BigDecimal("84.24"));
	}
	
	@Test
	public void testPrio5() throws ScriptException, CalculationException {
		when(parcelCalculationRepository.findAllByOrderByPriorityAsc()).thenReturn(createParcelRules());
		when(myntVoucherAdapter.getDiscount(VoucherCode.MYNT)).thenReturn(buildMyntVoucherCodeNotExpired());
		ParcelCalculationRequest parcelRequest = new ParcelCalculationRequest();
		parcelRequest.setHeight(new BigDecimal("1"));
		parcelRequest.setLength(new BigDecimal("2500"));
		parcelRequest.setVoucherCode(VoucherCode.MYNT);
		parcelRequest.setWeight(new BigDecimal("2"));
		parcelRequest.setWidth(new BigDecimal("1"));
		
		ParcelCalculationResponse resp = (ParcelCalculationResponse)parcelCalculatorServiceImpl.calculate(parcelRequest);
		
		Assertions.assertThat(resp.getCostOfDelivery()).isEqualTo(new BigDecimal("109.69"));
	}
	
	@Test
	public void testPrio5GFIcode() throws ScriptException, CalculationException {
		when(parcelCalculationRepository.findAllByOrderByPriorityAsc()).thenReturn(createParcelRules());
		when(myntVoucherAdapter.getDiscount(VoucherCode.GFI)).thenReturn(buildGFIVoucherCodeNotExpired());
		ParcelCalculationRequest parcelRequest = new ParcelCalculationRequest();
		parcelRequest.setHeight(new BigDecimal("1"));
		parcelRequest.setLength(new BigDecimal("2500"));
		parcelRequest.setVoucherCode(VoucherCode.GFI);
		parcelRequest.setWeight(new BigDecimal("2"));
		parcelRequest.setWidth(new BigDecimal("1"));
		
		ParcelCalculationResponse resp = (ParcelCalculationResponse)parcelCalculatorServiceImpl.calculate(parcelRequest);
		
		Assertions.assertThat(resp.getCostOfDelivery()).isEqualTo(new BigDecimal("115.63"));
	}
	
	@Test
	public void testPrio5ExpiredDiscount() throws ScriptException, CalculationException {
		when(parcelCalculationRepository.findAllByOrderByPriorityAsc()).thenReturn(createParcelRules());
		when(myntVoucherAdapter.getDiscount(VoucherCode.MYNT)).thenReturn(buildMyntVoucherCodeExpired());
		ParcelCalculationRequest parcelRequest = new ParcelCalculationRequest();
		parcelRequest.setVoucherCode(VoucherCode.MYNT);
		parcelRequest.setHeight(new BigDecimal("1"));
		parcelRequest.setLength(new BigDecimal("2500"));
		parcelRequest.setWeight(new BigDecimal("2"));
		parcelRequest.setWidth(new BigDecimal("1"));
		
		ParcelCalculationResponse resp = (ParcelCalculationResponse)parcelCalculatorServiceImpl.calculate(parcelRequest);
		
		Assertions.assertThat(resp.getCostOfDelivery()).isEqualTo(new BigDecimal("125.00"));
	}
	
	@Test
	public void testPrio2WithoutDiscount() throws ScriptException, CalculationException {
		when(parcelCalculationRepository.findAllByOrderByPriorityAsc()).thenReturn(createParcelRules());
		
		ParcelCalculationRequest parcelRequest = new ParcelCalculationRequest();
		parcelRequest.setHeight(new BigDecimal("20"));
		parcelRequest.setLength(new BigDecimal("20"));
		parcelRequest.setWeight(new BigDecimal("11"));
		parcelRequest.setWidth(new BigDecimal("20"));
		
		ParcelCalculationResponse resp = (ParcelCalculationResponse)parcelCalculatorServiceImpl.calculate(parcelRequest);
		
		Assertions.assertThat(resp.getCostOfDelivery()).isEqualTo(new BigDecimal("220.00"));
	}
	
	@Test
	public void testPrio3WithoutDiscount() throws ScriptException, CalculationException {
		when(parcelCalculationRepository.findAllByOrderByPriorityAsc()).thenReturn(createParcelRules());
	
		ParcelCalculationRequest parcelRequest = new ParcelCalculationRequest();
		parcelRequest.setHeight(new BigDecimal("2"));
		parcelRequest.setLength(new BigDecimal("500"));
		parcelRequest.setWeight(new BigDecimal("2"));
		parcelRequest.setWidth(new BigDecimal("1"));
		
		ParcelCalculationResponse resp = (ParcelCalculationResponse)parcelCalculatorServiceImpl.calculate(parcelRequest);
		
		Assertions.assertThat(resp.getCostOfDelivery()).isEqualTo(new BigDecimal("30.00"));
	}
	
	@Test
	public void testPrio4WithoutDiscount() throws ScriptException, CalculationException {
		when(parcelCalculationRepository.findAllByOrderByPriorityAsc()).thenReturn(createParcelRules());
		
		ParcelCalculationRequest parcelRequest = new ParcelCalculationRequest();
		parcelRequest.setHeight(new BigDecimal("1"));
		parcelRequest.setLength(new BigDecimal("2400"));
		parcelRequest.setWeight(new BigDecimal("2"));
		parcelRequest.setWidth(new BigDecimal("1"));
		
		ParcelCalculationResponse resp = (ParcelCalculationResponse)parcelCalculatorServiceImpl.calculate(parcelRequest);
		
		Assertions.assertThat(resp.getCostOfDelivery()).isEqualTo(new BigDecimal("96.00"));
	}
	
	@Test
	public void testPrio5WithoutDiscount() throws ScriptException, CalculationException {
		when(parcelCalculationRepository.findAllByOrderByPriorityAsc()).thenReturn(createParcelRules());
	
		ParcelCalculationRequest parcelRequest = new ParcelCalculationRequest();
		parcelRequest.setHeight(new BigDecimal("1"));
		parcelRequest.setLength(new BigDecimal("2500"));
		parcelRequest.setWeight(new BigDecimal("2"));
		parcelRequest.setWidth(new BigDecimal("1"));
		
		ParcelCalculationResponse resp = (ParcelCalculationResponse)parcelCalculatorServiceImpl.calculate(parcelRequest);
		
		Assertions.assertThat(resp.getCostOfDelivery()).isEqualTo(new BigDecimal("125.00"));
	}
}
