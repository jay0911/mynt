package com.mynt.exam;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.script.ScriptException;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.mynt.exam.dto.request.ParcelCalculationCmsEditRequest;
import com.mynt.exam.dto.request.ParcelCalculationCmsSaveRequest;
import com.mynt.exam.exception.CmsException;
import com.mynt.exam.model.ParcelCalculation;
import com.mynt.exam.repository.ParcelCalculationRepository;
import com.mynt.exam.service.impl.ParcelCalculationCmsServiceImpl;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ParcelCalculationCmsTest {
	
	@InjectMocks
	private ParcelCalculationCmsServiceImpl parcelCalculationCmsServiceImpl;

	@Mock
	private ParcelCalculationRepository parcelCalculationRepository;
	
	private List<ParcelCalculation> createListParcelRules(){
		ParcelCalculation reject = new ParcelCalculation(1, "Reject", "{WT} > 50", "N/A"); 
		reject.setDtimeCreated(new Date());
		reject.setDtimeUpdated(new Date());
		reject.setId("db957f40-4c69-4ef0-9849-58ee326e6e31");
		List<ParcelCalculation> parcels = new ArrayList<>();
		parcels.add(reject);
		return parcels;
	}
	
	private ParcelCalculation createParcelRules(){
		ParcelCalculation reject = new ParcelCalculation(1, "Reject", "{WT} > 50", "N/A"); 
		reject.setDtimeCreated(new Date());
		reject.setDtimeUpdated(new Date());
		reject.setId("db957f40-4c69-4ef0-9849-58ee326e6e31");
		return reject;
	}
	
	private ParcelCalculationCmsSaveRequest buildSaveRequest() {
		ParcelCalculationCmsSaveRequest req = new ParcelCalculationCmsSaveRequest();
		req.setCondition("{WT} > 50");
		req.setCost("N/A");
		req.setPriority(1);
		req.setRuleName("Reject");
		return req;
	}
	
	private ParcelCalculationCmsEditRequest buildEditRequestScriptError() {
		ParcelCalculationCmsEditRequest req = new ParcelCalculationCmsEditRequest();
		req.setCondition("{WT} > sfsfds 50");
		req.setCost("N/A");
		req.setPriority(1);
		req.setRuleName("Reject");
		req.setId("db957f40-4c69-4ef0-9849-58ee326e6e31");
		return req;
	}
	
	private ParcelCalculationCmsEditRequest buildEditRequest() {
		ParcelCalculationCmsEditRequest req = new ParcelCalculationCmsEditRequest();
		req.setCondition("{WT} > 50");
		req.setCost("N/A");
		req.setPriority(1);
		req.setRuleName("Reject");
		req.setId("db957f40-4c69-4ef0-9849-58ee326e6e31");
		return req;
	}
	
	private ParcelCalculationCmsSaveRequest buildSaveRequestErrorScript() {
		ParcelCalculationCmsSaveRequest req = new ParcelCalculationCmsSaveRequest();
		req.setCondition("{WT} d > 50");
		req.setCost("N/A");
		req.setPriority(1);
		req.setRuleName("Reject");
		return req;
	}
	
	@Test
	public void listParcel() {
		when(parcelCalculationRepository.findAll()).thenReturn(createListParcelRules());
		Assertions.assertThat(parcelCalculationCmsServiceImpl.findAll()).isNotEmpty();
	}
	
	@Test
	public void saveParcel() throws ScriptException {
		Assertions.assertThat(parcelCalculationCmsServiceImpl.save(buildSaveRequest())).isEqualTo(true);
	}
	
	@Test(expected=ScriptException.class)
	public void saveParcelErrorOnScript() throws ScriptException {
		Assertions.assertThat(parcelCalculationCmsServiceImpl.save(buildSaveRequestErrorScript())).isEqualTo(true);
	}
	
	@Test
	public void editParcel() throws ScriptException, CmsException {
		when(parcelCalculationRepository.findById("db957f40-4c69-4ef0-9849-58ee326e6e31")).thenReturn(Optional.of(createParcelRules()));
		Assertions.assertThat(parcelCalculationCmsServiceImpl.edit(buildEditRequest())).isEqualTo(true);
	}
	
	@Test(expected=CmsException.class)
	public void editParcelThrowCmsException() throws ScriptException, CmsException {
		Assertions.assertThat(parcelCalculationCmsServiceImpl.edit(buildEditRequest())).isEqualTo(true);
	}
	
	@Test(expected=ScriptException.class)
	public void editParcelThrowScriptException() throws ScriptException, CmsException {
		when(parcelCalculationRepository.findById("db957f40-4c69-4ef0-9849-58ee326e6e31")).thenReturn(Optional.of(createParcelRules()));
		Assertions.assertThat(parcelCalculationCmsServiceImpl.edit(buildEditRequestScriptError())).isEqualTo(true);
	}
	
	@Test
	public void deleteParcel() throws ScriptException, CmsException {
		when(parcelCalculationRepository.findById("db957f40-4c69-4ef0-9849-58ee326e6e31")).thenReturn(Optional.of(createParcelRules()));
		Assertions.assertThat(parcelCalculationCmsServiceImpl.delete("db957f40-4c69-4ef0-9849-58ee326e6e31")).isEqualTo(true);
	}
	
	@Test(expected=CmsException.class)
	public void deleteParcelIdNotFound() throws ScriptException, CmsException {
		Assertions.assertThat(parcelCalculationCmsServiceImpl.delete("r43242")).isEqualTo(true);
	}
	
}
